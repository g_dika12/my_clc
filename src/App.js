import { NavigationContainer } from '@react-navigation/native';
import Axios from 'axios';
import React, { memo, useEffect, useRef, useState } from 'react';
import { AppState } from 'react-native';
import FlashMessage from 'react-native-flash-message';
import 'react-native-gesture-handler';
import { Provider, useSelector } from 'react-redux';
import { Loading } from './components';
import store from './redux/store';
import Router from './router';
import storage from './utils/storage';

const MainApp = () => {
  const { isLoading } = useSelector((state) => state.globalReducer);
  // console.log({isLoading})// isLoading == false
  return (
    <NavigationContainer>
      <Router />
      <FlashMessage position="top" />
      {isLoading && <Loading />}
    </NavigationContainer>
  );
};

const App = memo(() => {
  const appState = useRef(AppState.currentState); // -->> _handleAppStateChange
  // foreground = 'active' = user menggunakan apps
  // background = device user = home // user menggunakan apps lain 
  // mengembalikan ref yang sama dengan component (saat komponen tersebut pertama rendering)
  // useRef artinya mengarahkan user langsung ke komponen yang udah di set useRef
  const [appStateVisible, setAppStateVisible] = useState(appState.current);
  const [refreshToken, setRefreshToken] = useState('');
  const [token, setToken] = useState('');

  const _handleAppStateChange = (nextAppState) => {
    // if (
    //   appState.current.match(/inactive|background/) &&
    //   nextAppState === 'active'
    // ) {
    //   console.log('App has come to the foreground!');
    // }

    appState.current = nextAppState;
    setAppStateVisible(appState.current);
  };

  useEffect(() => {
    // console.log('APP STATE . CURRENT', appState.current) // active
    AppState.addEventListener('change', _handleAppStateChange); // mengaktifkan fungsi _handle....
    // method untuk membuat event 
    if (appStateVisible === 'active') {
      storage
        .load({
          key: 'refreshToken',
          autoSync: true,
          syncInBackground: true,
          syncParams: {
            someFlag: true,
          },
        })
        .then((ret) => {
          // console.log({ret}) // data token
          setRefreshToken(ret);
        });

      const data = {
        refreshToken: refreshToken,
      };

      Axios.post(
        'https://api.laporanclcsmp.com/api/v1/auth/refreshToken',
        data,
      ).then((res) => {
        console.log('= ',res.data.data.token)
        storage.save({
          key: 'token',
          data: res.data.data.token,// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjEsImlhdCI6MTYxNzk4ODExNX0.rxVKLTqkQkEzRJz4Yns8DOjt5Orx-rp5N94bJm3fL2Y
        });
        storage
          .load({
            key: 'token',
            autoSync: true,
            syncInBackground: true,
            syncParams: {
              someFlag: true,
            },
          })
          .then((ret) => {
            setToken(ret);
          });
      });
    }

    return () => {
      AppState.removeEventListener('change', _handleAppStateChange); // menghentikan fungsi _handleAppState....
    };
  }, []);

  return (
    <Provider store={store}>
      <MainApp />
    </Provider>
  );
});

export default App;
