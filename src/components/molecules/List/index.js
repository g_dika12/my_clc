import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {IcArrowRight} from '../../../assets';
import normalize from 'react-native-normalize';

const List = ({name, price, onPress}) => {
  return (
    <View>
      <TouchableOpacity
        style={styles.list}
        activeOpacity={0.7}
        onPress={onPress}>
        <View style={styles.container}>
          <Text style={styles.file}>{name}</Text>
          <Text style={styles.price}>{price}</Text>
        </View>
        <IcArrowRight />
      </TouchableOpacity>
    </View>
  );
};

export default List;

const styles = StyleSheet.create({
  list: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 2,
    borderColor: '#C4C4C4',
    marginHorizontal: normalize(35),
    paddingVertical: normalize(20),
    paddingLeft: normalize(14),
    paddingRight: normalize(20),
  },
  container: {
    flex: 1,
  },
  icon: {
    width: normalize(50),
    height: normalize(50),
    marginRight: normalize(20),
  },
  file: {
    fontFamily: 'Montserrat-Medium',
    fontSize: normalize(16),
    color: '#2E2E2E',
  },
  status: {
    fontFamily: 'Montserrat-Medium',
    fontSize: normalize(10),
    color: '#2E2E2E',
  },
  border: {
    borderWidth: 1,
    borderColor: '#C4C4C4',
    marginHorizontal: normalize(35),
  },
  price: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(14),
    color: '#181818',
  },
});
