import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {IcArrowLeft} from '../../../assets';
import normalize from 'react-native-normalize';

const Header = ({title, onBack, onPress}) => {
  return (
    <View style={styles.container}>
      {onBack && (
        <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
          <View style={styles.back}>
            <IcArrowLeft />
          </View>
        </TouchableOpacity>
      )}
      <View style={styles.content}>
        <Text style={styles.title}>{title}</Text>
      </View>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    paddingHorizontal: normalize(20),
    paddingTop: normalize(30),
    paddingBottom: normalize(24),
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: normalize(23),
    color: '#2E2E2E',
  },
  back: {
    padding: normalize(16),
    marginRight: normalize(10),
    marginLeft: normalize(-16),
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
