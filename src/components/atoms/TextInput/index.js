import React from 'react';
import {StyleSheet, TextInput as TextInputRN, View} from 'react-native';
import normalize from 'react-native-normalize';

const TextInput = ({label, placeholder, disabled, ...restProps}) => {
  return (
    <View>
      <TextInputRN
        style={styles.input}
        placeholder={placeholder}
        editable={disabled}
        {...restProps}
      />
    </View>
  );
};

export default TextInput;

const styles = StyleSheet.create({
  input: {
    fontFamily: 'Montserrat-Medium',
    fontSize: normalize(16),
    borderBottomWidth: 1,
    borderColor: '#A8A8A8',
    paddingBottom: normalize(14),
    paddingHorizontal: normalize(14),
    marginBottom: normalize(15),
  },
});
