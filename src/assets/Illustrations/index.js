import Logo from './Logo.png';
import ILApproved from './ILApproved.png';
import ILWaiting from './ILWaiting.png';
import ILRejected from './ILRejected.png';

export {Logo, ILApproved, ILRejected, ILWaiting};
