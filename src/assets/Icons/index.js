import IcHome from './IcHome.svg';
import IcHomeActive from './IcHomeActive.svg';
import IcProfile from './IcProfile.svg';
import IcProfileActive from './IcProfileActive.svg';
import IcDataPribadi from './IcDataPribadi.svg';
import IcSettings from './IcSettings.svg';
import IcInformation from './IcInfomation.svg';
import IcArrowRight from './IcArrowRight.svg';
import IcArrowDown from './IcArrowDown.svg';
import IcArrowLeft from './IcArrowLeft.svg';
import IcLogout from './IcLogout.svg';
import IcRABActive from './IcRABActive.svg';
import IcRAB from './IcRAB.svg';
import IcEdit from './IcEdit.svg';
import IcEditActive from './IcEditActive.svg';
import IcPengeluaran from './IcPengeluaran.svg';
import IcPengeluaranActive from './IcPengeluaranActive.svg';
import IcCamera from './IcCamera.svg';

export {
  IcHome,
  IcHomeActive,
  IcProfile,
  IcProfileActive,
  IcDataPribadi,
  IcSettings,
  IcInformation,
  IcArrowRight,
  IcArrowDown,
  IcArrowLeft,
  IcLogout,
  IcRAB,
  IcRABActive,
  IcEdit,
  IcEditActive,
  IcPengeluaran,
  IcPengeluaranActive,
  IcCamera,
};
