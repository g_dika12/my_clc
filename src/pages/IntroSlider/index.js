import React, {useState} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import storage from '../../utils/storage';

const IntroSlider = ({navigation}) => {
  const [showRealApp] = useState(false);
  const slides = [
    {
      key: 'one',
      title: 'Volutpat amet fringilla \n elementum  id.',
      desc:
        'Lorem ipsum dolor sit amet, \n consectetur adipiscing elit. Iaculis \n amet.',
      image: require('../../assets/Illustrations/Slider1.png'),
    },
    {
      key: 'two',
      title: 'Volutpat amet fringilla \n elementum  id.',
      desc:
        'Lorem ipsum dolor sit amet, \n consectetur adipiscing elit. Iaculis \n amet.',
      image: require('../../assets/Illustrations/Slider2.png'),
    },
    {
      key: 'three',
      title: 'Volutpat amet fringilla \n elementum  id.',
      desc:
        'Lorem ipsum dolor sit amet, \n consectetur adipiscing elit. Iaculis \n amet.',
      image: require('../../assets/Illustrations/Slider3.png'),
    },
  ];

  const renderItem = ({item}) => {
    return (
      <View style={styles.slide}>
        <Image source={item.image} style={styles.image} />
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.desc}>{item.desc}</Text>
      </View>
    );
  };

  const renderSkipButton = () => {
    return <Text style={styles.skip}>Skip</Text>;
  };
  const renderNextButton = () => {
    return <Text style={styles.next}>Next</Text>;
  };
  const renderDoneButton = () => {
    return (
      <View style={styles.doneButton}>
        <Text style={styles.done}>Lanjut</Text>
      </View>
    );
  };

  const onDone = () => {
    storage.save({
      key: 'firstTime',
      data: {
        status: 'No',
      },
    });
    navigation.replace('Login');
  };

  if (showRealApp === false) {
    return (
      <AppIntroSlider
        renderItem={renderItem}
        data={slides}
        showSkipButton
        showNextButton
        activeDotStyle={styles.activeDotStyle}
        dotStyle={styles.dotStyle}
        renderSkipButton={renderSkipButton}
        renderNextButton={renderNextButton}
        renderDoneButton={renderDoneButton}
        onDone={onDone}
      />
    );
  }
};

export default IntroSlider;

const styles = StyleSheet.create({
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
  image: {
    width: 277,
    height: 234,
    marginTop: -30,
    marginBottom: 68,
  },
  title: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 22,
    color: '#2E2E2E',
    textAlign: 'center',
    marginBottom: 23,
  },
  desc: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 16,
    color: '#2E2E2E',
    textAlign: 'center',
  },
  dotStyle: {
    backgroundColor: 'rgba(0, 0, 0, .2)',
    marginBottom: 142,
  },
  activeDotStyle: {
    backgroundColor: '#FED330',
    width: 25,
    marginBottom: 142,
  },
  skip: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 17,
    color: '#000000',
    paddingLeft: 35,
  },
  next: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 17,
    color: '#000000',
    paddingRight: 35,
  },
  doneButton: {
    backgroundColor: '#FED330',
    borderRadius: 10,
    paddingVertical: 17,
    paddingHorizontal: 158,
    marginTop: -20,
    elevation: 1,
  },
  done: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 18,
    color: '#FFFFFF',
  },
});
