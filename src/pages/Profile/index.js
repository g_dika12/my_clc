import React, {useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {
  IcArrowRight,
  IcDataPribadi,
  IcInformation,
  IcLogout,
} from '../../assets';
import storage from '../../utils/storage';

const Profile = ({navigation}) => {
  const [nama, setNama] = useState('');
  const [npsn, setNpsn] = useState('');
  storage
    .load({
      key: 'profile',
    })
    .then((ret) => {
      setNama(ret.user_nama);
      setNpsn(ret.cabang.kode);
    })
    .catch((err) => {
      console.warn(err.message);
    });
  const onLogout = () => {
    storage.remove({
      key: 'token',
    });
    storage.remove({
      key: 'profile',
    });
    storage.remove({
      key: 'refreshToken',
    });
    navigation.reset({index: 0, routes: [{name: 'Login'}]});
  };
  return (
    <View style={styles.page}>
      <View style={styles.header}>
        <View>
          <Text style={styles.title}>{nama}</Text>
          <Text style={styles.subTitle}>{npsn}</Text>
        </View>
      </View>
      <TouchableOpacity
        style={styles.listMenu}
        activeOpacity={0.7}
        onPress={() => navigation.navigate('EditProfile')}>
        <IcDataPribadi />
        <View style={styles.listContainer}>
          <Text style={styles.listTitle}>Data Pribadi</Text>
        </View>
        <IcArrowRight />
      </TouchableOpacity>
      <View style={styles.listMenu}>
        <IcInformation />
        <View style={styles.listContainer}>
          <Text style={styles.listTitle}>Informasi Aplikasi</Text>
        </View>
        <IcArrowRight />
      </View>
      <TouchableOpacity
        style={styles.button}
        activeOpacity={0.7}
        onPress={onLogout}>
        <IcLogout />
        <Text style={styles.text}>Logout</Text>
      </TouchableOpacity>
      <View style={styles.containerVersion}>
        <Text style={styles.version}>Versi 1.0.0</Text>
      </View>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 48,
    paddingHorizontal: 30,
  },
  profile: {
    width: 80,
    height: 80,
    borderRadius: 40,
    marginRight: 20,
  },
  title: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 20,
    color: '#2E2E2E',
  },
  subTitle: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 14,
    color: '#2E2E2E',
  },
  listMenu: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: 51,
    marginRight: 43,
    marginBottom: 30,
  },
  listContainer: {
    flex: 1,
    marginLeft: 22,
  },
  listTitle: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 17,
    color: '#000000',
  },
  button: {
    flexDirection: 'row',
    backgroundColor: '#181818',
    marginHorizontal: 45,
    marginVertical: 12,
    paddingVertical: 11,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
  },
  text: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 14,
    color: '#FFFFFF',
    marginLeft: 13,
  },
  containerVersion: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  version: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 14,
    color: '#6D6D6D',
    textAlign: 'center',
    marginBottom: 38,
  },
});
