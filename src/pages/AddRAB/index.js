import Axios from 'axios';
import React, { memo, useEffect, useMemo, useState } from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import normalize from 'react-native-normalize';
import { Button, Header, TextInput } from '../../components';
import { useForm } from '../../utils';
import storage from '../../utils/storage';

const AddRAB = ({ navigation }) => {
  const [token, setToken] = useState('');
  const [activitas, setActivitas] = useState([]);
  const [activitas1, setActivitas1] = useState([]);
  const [activitas2, setActivitas2] = useState([]);
  const [activitas3, setActivitas3] = useState([]);
  const [activitas4, setActivitas4] = useState([]);
  const [selectedActivitas, setSelectedActivitas] = useState('');
  const [selectedActivitas1, setSelectedActivitas1] = useState('');
  const [selectedActivitas2, setSelectedActivitas2] = useState('');
  const [selectedActivitas3, setSelectedActivitas3] = useState('');
  const [selectedActivitas4, setSelectedActivitas4] = useState('');
  const [status, setStatus] = useState(false)

  const [form, setForm] = useForm({
    kode: '',
    uraian: '',
    jumlah_1: '',
    jumlah_2: '',
    jumlah_3: '',
    jumlah_4: '',
    satuan_1: '',
    satuan_2: '',
    satuan_3: '',
    satuan_4: '',
    harga_ringgit: '',
    harga_rupiah: '',
    total_harga_ringgit: '',
    total_harga_rupiah: '',
    prioritas: '',
  });

  const API_HOST = {
    url: 'https://api.laporanclcsmp.com/api/v1/',
  };

  // const getData = async () => {
  //   const response = await Axios.get(`${API_HOST.url}/aktifitas`, {
  //     headers: {
  //       Authorization: `Bearer ${token}`,
  //     },
  //   });
  //   setActivitas(response.data.aktifitas);
  //   getData1(selectedActivitas);
  // };

  const newToken = `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjEsImlhdCI6MTYxNzk4ODExNX0.rxVKLTqkQkEzRJz4Yns8DOjt5Orx-rp5N94bJm3fL2Y`
  // const _ContentRequestMethod = useMemo(
  //   () => ({
  //
  const getData =  async() => {
    try {
      const response = await Axios.get(`${API_HOST.url}/aktifitas`, {
        headers: {
          Authorization: `Bearer ${newToken}`,
        },
      });
      setActivitas([...response.data.aktifitas]);
      await getData1(selectedActivitas);
    } catch (error) {
      console.log('error getData : ', error)
    }
  };

  const getData1 = async (selected) => {
    console.log('GET 1 RUN')
    console.log({selected})
    try {
      const response = await Axios.get(
        `${API_HOST.url}/aktifitas/${selectedActivitas}`,
        {
          headers: {
            Authorization: `Bearer ${newToken}`,
          },
        },
      );
      console.log('GET 1 = ', JSON.stringify(response, null, 2))
      setActivitas1([...response.data.aktifitas]);
      // getData2(selectedActivitas1);
    } catch (error) {
      console.log('error getData 1: ', error)
    }
  }

  //
  //   })
  // )

  // const getData1 = (selected) => {
  //   const response = Axios.get(
  //     `${API_HOST.url}/aktifitas/${selectedActivitas}`,
  //     {
  //       headers: {
  //         Authorization: `Bearer ${token}`,
  //       },
  //     },
  //   );
  //   setActivitas1(response.data.aktifitas);
  //   getData2(selectedActivitas1);
  // };

  const getData2 = (selected) => {
    const response = Axios.get(
      `${API_HOST.url}/aktifitas2/${selectedActivitas1}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    setActivitas2(response.data.aktifitas);
    setForm('kode', response.data.code);
    getData3(selectedActivitas2);
  };

  const getData3 = (selected) => {
    const response = Axios.get(
      `${API_HOST.url}/aktifitas3/${selectedActivitas2}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    setActivitas3(response.data.aktifitas);
    getData4(selectedActivitas3);
    setForm('kode', response.data.code);
  };

  const getData4 = (selected) => {
    const response = Axios.get(
      `${API_HOST.url}/aktifitas4/${selectedActivitas3}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    setActivitas4(response.data.aktifitas);
    getCode(selectedActivitas4);
    // setForm('kode', response.data.code);
  };

  const getCode = (selected) => {
    const response = Axios.get(
      `${API_HOST.url}/code/${selectedActivitas4}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    console.log(response);
    // setForm('kode', response.data.code);
  };

  console.log(form.kode);

  useEffect(() => {
    storage
      .load({
        key: 'token',
      })
      .then((resToken) => {
        setToken(resToken);
        getData();
      })
      .catch((err) => {
        console.warn(err.message);
      });
  }, []);
  console.log('- - ')
  console.log('++', selectedActivitas)
  return (
    <View style={styles.page}>
      <Header title="Tambah RAB" onBack onPress={() => navigation.goBack()} />
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.content}>
            <DropDownPicker
              items={activitas}
              defaultValue={selectedActivitas}
              style={styles.picker}
              itemStyle={styles.itemStyle}
              selectedLabelStyle={styles.labelActive}
              placeholderStyle={styles.placeholder}
              placeholder="Pilih Uraian"
              onChangeItem={
                (item) => { 
                  console.log('>> ',item.value)
                  const data = item.label;
                  console.log('>>>>>',data)
                setSelectedActivitas(data.value)
              }
              }
            />
            <DropDownPicker
              items={activitas1}
              defaultValue={selectedActivitas1}
              style={styles.picker}
              itemStyle={styles.itemStyle}
              selectedLabelStyle={styles.labelActive}
              placeholderStyle={styles.placeholder}
              placeholder="Pilih Uraian"
              onChangeItem={(item) => setSelectedActivitas1(item.value)}
            />
            <DropDownPicker
              items={activitas2}
              defaultValue={selectedActivitas2}
              style={styles.picker}
              itemStyle={styles.itemStyle}
              selectedLabelStyle={styles.labelActive}
              placeholderStyle={styles.placeholder}
              placeholder="Pilih Uraian"
              onChangeItem={(item) => setSelectedActivitas2(item.value)}
            />
            <DropDownPicker
              items={activitas3}
              defaultValue={selectedActivitas3}
              style={styles.picker}
              itemStyle={styles.itemStyle}
              selectedLabelStyle={styles.labelActive}
              placeholderStyle={styles.placeholder}
              placeholder="Pilih Uraian"
              onChangeItem={(item) => setSelectedActivitas3(item.value)}
            />
            <DropDownPicker
              items={activitas4}
              defaultValue={selectedActivitas4}
              style={styles.picker}
              itemStyle={styles.itemStyle}
              selectedLabelStyle={styles.labelActive}
              placeholderStyle={styles.placeholder}
              placeholder="Pilih Uraian"
              onChangeItem={(item) => setSelectedActivitas4(item.value)}
            />
            <TextInput
              placeholder="NPSN"
              value={form.kode}
              onChangeText={(value) => setForm('kode', value)}
            />
            <TextInput
              placeholder="Uraian"
              value={form.uraian}
              onChangeText={(value) => setForm('uraian', value)}
            />
            <View style={styles.smallForm}>
              <View style={styles.left}>
                <TextInput
                  placeholder="Jumlah"
                  value={form.jumlah_1}
                  onChangeText={(value) => setForm('jumlah_1', value)}
                />
              </View>
              <View style={styles.right}>
                <TextInput
                  placeholder="Satuan"
                  value={form.satuan_1}
                  onChangeText={(value) => setForm('satuan_1', value)}
                />
              </View>
            </View>
            <View style={styles.smallForm}>
              <View style={styles.left}>
                <TextInput
                  placeholder="Jumlah"
                  value={form.jumlah_2}
                  onChangeText={(value) => setForm('jumlah_2', value)}
                />
              </View>
              <View style={styles.right}>
                <TextInput
                  placeholder="Satuan"
                  value={form.satuan_2}
                  onChangeText={(value) => setForm('satuan_2', value)}
                />
              </View>
            </View>
            <View style={styles.smallForm}>
              <View style={styles.left}>
                <TextInput
                  placeholder="Jumlah"
                  value={form.jumlah_3}
                  onChangeText={(value) => setForm('jumlah_3', value)}
                />
              </View>
              <View style={styles.right}>
                <TextInput
                  placeholder="Satuan"
                  value={form.satuan_3}
                  onChangeText={(value) => setForm('satuan_3', value)}
                />
              </View>
            </View>
            <View style={styles.smallForm}>
              <View style={styles.left}>
                <TextInput
                  placeholder="Jumlah"
                  value={form.jumlah_4}
                  onChangeText={(value) => setForm('jumlah_4', value)}
                />
              </View>
              <View style={styles.right}>
                <TextInput
                  placeholder="Satuan"
                  value={form.satuan_4}
                  onChangeText={(value) => setForm('satuan_4', value)}
                />
              </View>
            </View>
            <TextInput
              placeholder="Harga Ringgit (RM)"
              value={form.harga_ringgit}
              onChangeText={(value) => setForm('harga_ringgit', value)}
            />
            <TextInput
              placeholder="Harga Rupiah (RP)"
              value={form.harga_rupiah}
              onChangeText={(value) => setForm('harga_rupiah', value)}
            />
            <TextInput placeholder="Total" />
            <TextInput
              placeholder="Total Ringgit (RM)"
              value={form.total_harga_ringgit}
              onChangeText={(value) => setForm('total_harga_ringgit', value)}
            />
            <TextInput
              placeholder="Total Rupiah (RP)"
              value={form.total_harga_rupiah}
              onChangeText={(value) => setForm('total_harga_rupiah', value)}
            />
            <TextInput
              placeholder="Prioritas"
              value={form.prioritas}
              onChangeText={(value) => setForm('prioritas', value)}
            />
          </View>
        </ScrollView>
        <View style={styles.button}>
          <Button text="Tambah" />
        </View>
      </View>
    </View>
  );
};

export default AddRAB;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  content: {
    flex: 1,
    paddingHorizontal: normalize(20),
    marginBottom: normalize(20),
  },
  label: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(12),
    color: '#595959',
  },
  input: {
    borderBottomWidth: 1,
    borderBottomColor: '#A8A8A8',
    marginBottom: normalize(15),
  },
  picker: {
    backgroundColor: '#FFFFFF',
    borderWidth: 0,
    borderBottomWidth: 1,
    borderBottomColor: '#A8A8A8',
    marginBottom: normalize(15),
  },
  itemStyle: {
    justifyContent: 'flex-start',
  },
  placeholder: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 16,
    color: '#6D6D6D',
  },
  labelActive: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 16,
    color: '#020202',
  },
  smallForm: {
    flexDirection: 'row',
  },
  left: {
    flex: 1,
    marginRight: normalize(10),
  },
  right: {
    flex: 1,
  },
  button: {
    marginHorizontal: normalize(20),
    marginBottom: normalize(67),
  },
});
