import React, {useEffect, useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {useSelector} from 'react-redux';
import {useDispatch} from 'react-redux';
import {ILApproved} from '../../assets';
import {Gap, Header, Number} from '../../components';
import {getSaldo} from '../../redux/action/auth';
import storage from '../../utils/storage';

const Pengeluaran = ({navigation}) => {
  const [rupiah, setRupiah] = useState(0);
  const dispatch = useDispatch();
  const [ringgit, setRinggit] = useState(0);
  const {saldoReducer} = useSelector((state) => state);

  useEffect(() => {
    setRupiah(saldoReducer.saldo.total_rupiah);
    setRinggit(saldoReducer.saldo.total_ringgit);
    storage
    .load({
      key: 'profile',
      autoSync: true,
      syncInBackground: true,
      syncParams: {
        someFlag: true,
      },
    })
    .then((result) => {
      const id = result.cabang.id;
      dispatch(getSaldo(id));
    })
    .catch((err) => {
      console.log(err);
    });
  }, [saldoReducer]);

  return (
    <View style={styles.page}>
      <Header title="Pengeluaran" />
      <View style={styles.container}>
        <View style={styles.left}>
          <Text style={styles.label}>Saldo (Rp)</Text>
          <Number number={rupiah} style={styles.amount} prefix="Rp. " />
        </View>
        <View style={styles.line} />
        <View style={styles.right}>
          <Text style={styles.label}>Saldo (RM)</Text>
          <Number number={ringgit} style={styles.amount} prefix="RM " />
        </View>
      </View>
      <View style={styles.content}>
        <View style={styles.card}>
          {/* <TouchableOpacity
            style={styles.add}
            activeOpacity={0.7}
            onPress={() => navigation.navigate('AddPengeluaran')}>
            <Text style={styles.addText}>Add</Text>
          </TouchableOpacity>
          <Gap width={20} /> */}
          <TouchableOpacity
            style={styles.check}
            activeOpacity={0.7}
            onPress={() => navigation.navigate('CheckSisaPengeluaran')}>
            <Text style={styles.requestText}>Check</Text>
          </TouchableOpacity>
          <Gap width={20} />
          <TouchableOpacity
            style={styles.request}
            activeOpacity={0.7}
            onPress={() => navigation.navigate('ListPengeluaran')}>
            <Text style={styles.requestText}>Request</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.title}>Pengeluaran</Text>
        <View style={styles.list}>
          <Image source={ILApproved} style={styles.icon} />
          <View>
            <Text style={styles.name}>Beli Buku</Text>
            <Text style={styles.jumlah}>Rp. 1.000.000</Text>
          </View>
        </View>
        <View style={styles.list}>
          <Image source={ILApproved} style={styles.icon} />
          <View>
            <Text style={styles.name}>Beli Meja</Text>
            <Text style={styles.jumlah}>Rp. 2.000.000</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Pengeluaran;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  container: {
    flexDirection: 'row',
    marginHorizontal: 20,
  },
  left: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
  },
  line: {
    width: 20,
    borderRightWidth: 1,
    borderRightColor: '#C4C4C4',
  },
  right: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  label: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
    color: '#202F45',
    marginBottom: 8,
  },
  amount: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    color: '#FED330',
  },
  content: {
    flex: 1,
    marginTop: 20,
  },
  card: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 20,
    marginVertical: 20,
  },
  add: {
    flex: 1,
    backgroundColor: '#181818',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 20,
  },
  addText: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 16,
    color: '#FFF',
  },
  request: {
    flex: 1,
    backgroundColor: '#181818',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 20,
  },
  check: {
    flex: 1,
    backgroundColor: '#181818',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 20,
  },
  requestText: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 16,
    color: '#FFF',
  },
  title: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 18,
    paddingHorizontal: 24,
  },
  list: {
    flexDirection: 'row',
    paddingLeft: 31,
    paddingRight: 51,
    alignItems: 'center',
    marginTop: 15,
  },
  icon: {
    width: 50,
    height: 50,
    marginRight: 20,
  },
  name: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
    color: '#000000',
  },
  jumlah: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 13,
    color: '#6D6D6D',
  },
});
