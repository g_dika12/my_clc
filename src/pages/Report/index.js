import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {ILApproved} from '../../assets';
import {Header} from '../../components';

const Report = ({navigation}) => {
  return (
    <View style={styles.page}>
      <Header
        title="Laporan terakhir"
        onBack
        onPress={() => navigation.goBack()}
      />
      <View style={styles.list}>
        <Image source={ILApproved} style={styles.icon} />
        <View>
          <Text style={styles.file}>RAB.xls</Text>
          <Text style={styles.date}>Senin, 01 Maret 2021</Text>
        </View>
      </View>
    </View>
  );
};

export default Report;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  list: {
    flexDirection: 'row',
    paddingLeft: 31,
    paddingRight: 51,
    alignItems: 'center',
  },
  icon: {
    width: 50,
    height: 50,
    marginRight: 20,
  },
  file: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
    color: '#000000',
  },
  date: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 13,
    color: '#6D6D6D',
  },
});
