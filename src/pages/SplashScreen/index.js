import React, {useEffect} from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {Logo} from '../../assets';
import storage from '../../utils/storage';

const SplashScreen = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      storage
        .load({
          key: 'firstTime',
          autoSync: true,
          syncInBackground: true,
          syncParams: {
            someFlag: true,
          },
        })
        .then((ret) => {
          if (ret.status === 'No') {
            storage
              .load({
                key: 'token',
                autoSync: true,
                syncInBackground: true,
                syncParams: {
                  someFlag: true,
                },
              })
              .then((result) => {
                if (result) {
                  navigation.reset({index: 0, routes: [{name: 'MainApp'}]});
                }
              })
              .catch(() => {
                navigation.replace('Login');
              });
          } else {
            navigation.replace('IntroSlider');
          }
        })
        .catch(() => {
          navigation.replace('IntroSlider');
        });
    }, 2000);
  }, [navigation]);
  return (
    <View style={styles.page}>
      <Image source={Logo} style={styles.logo} />
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  page: {
    backgroundColor: '#FED330',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: 155,
    height: 147,
  },
});
