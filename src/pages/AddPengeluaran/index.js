import DateTimePicker from '@react-native-community/datetimepicker';
import {Picker} from '@react-native-picker/picker';
import Moment from 'moment';
import 'moment/locale/id';
import React, {useState} from 'react';
import {
  Animated,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {launchCamera} from 'react-native-image-picker';
import {IcCamera} from '../../assets';
import {Button, Gap, Header, TextInput} from '../../components';
import {showMessage, useForm} from '../../utils';
import normalize from 'react-native-normalize';

const AddPengeluaran = ({navigation}) => {
  const [selectedLanguage, setSelectedLanguage] = useState();
  const [form, setForm] = useForm({
    date: new Date(),
    photo: '',
  });
  const [show, setShow] = useState(false);
  const [photo, setPhoto] = useState(null);

  const onChange = (selectedDate) => {
    const currentDate = selectedDate || form.date;
    setForm('date', currentDate);
    setShow(false);
  };

  const openCamera = () => {
    launchCamera(
      {
        quality: 1,
        maxHeight: 2000,
        maxWidth: 2000,
      },
      (response) => {
        if (response.didCancel || response.error) {
          showMessage('Anda tidak memilih Photo', 'failed');
        } else {
          setPhoto(response);
          setForm('photo', response);
        }
      },
    );
  };

  return (
    <View style={styles.page}>
      <Header
        title="Tambah Pengeluaran"
        onBack
        onPress={() => navigation.goBack()}
      />
      <View style={styles.container}>
        <View style={styles.containerAnggaran}>
          <Text style={styles.labelAnggaran}>Dana Anggaran</Text>
          <Text style={styles.anggaranAmount}>Rp. 100.000.000</Text>
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.input}>
            <Picker
              style={styles.picker}
              selectedValue={selectedLanguage}
              onValueChange={(itemValue, itemIndex) =>
                setSelectedLanguage(itemValue)
              }
              mode="dropdown">
              <Picker.Item label="Java" value="java" />
              <Picker.Item label="JavaScript" value="js" />
            </Picker>
          </View>
          <View style={styles.input}>
            <Picker
              style={styles.picker}
              selectedValue={selectedLanguage}
              onValueChange={(itemValue, itemIndex) =>
                setSelectedLanguage(itemValue)
              }
              mode="dropdown">
              <Picker.Item label="Java" value="java" />
              <Picker.Item label="JavaScript" value="js" />
            </Picker>
          </View>
          <View style={styles.input}>
            <Picker
              style={styles.picker}
              selectedValue={selectedLanguage}
              onValueChange={(itemValue, itemIndex) =>
                setSelectedLanguage(itemValue)
              }
              mode="dropdown">
              <Picker.Item label="Java" value="java" />
              <Picker.Item label="JavaScript" value="js" />
            </Picker>
          </View>
          <View style={styles.input}>
            <Picker
              style={styles.picker}
              selectedValue={selectedLanguage}
              onValueChange={(itemValue, itemIndex) =>
                setSelectedLanguage(itemValue)
              }
              mode="dropdown">
              <Picker.Item label="Java" value="java" />
              <Picker.Item label="JavaScript" value="js" />
            </Picker>
          </View>
          <TextInput placeholder="NPSN" editable={false} />
          <TextInput placeholder="Masukkan Nama Toko" />
          <TextInput placeholder="Keterangan" />
          <TouchableOpacity
            style={styles.calendar}
            onPress={() => setShow(true)}>
            <Text style={styles.date}>
              {Moment(form.date).format('DD/MM/YYYY')}
            </Text>
            {show && (
              <DateTimePicker
                testID="dateTimePicker"
                value={form.date}
                mode="date"
                is24Hour={true}
                display="default"
                onChange={onChange}
              />
            )}
          </TouchableOpacity>
          <TextInput placeholder="Total Harga" />
          {photo && (
            <Animated.View style={styles.photoContainer}>
              <Image source={photo} style={styles.photo} />
            </Animated.View>
          )}
          <Button
            color="#FED330"
            text="Ambil Foto"
            icon={<IcCamera />}
            onPress={openCamera}
          />

          <Gap height={40} />
          <Button text="Simpan" />
          <Gap height={20} />
        </ScrollView>
      </View>
    </View>
  );
};

export default AddPengeluaran;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  container: {
    flex: 1,
    justifyContent: 'space-between',
    marginHorizontal: normalize(20),
  },
  form: {
    backgroundColor: '#FEF7DD',
    paddingVertical: normalize(16),
    borderRadius: normalize(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerAnggaran: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: normalize(10),
  },
  labelAnggaran: {
    fontFamily: 'Montserrat-Medium',
    fontSize: normalize(18),
    color: '#181818',
  },
  anggaranAmount: {
    fontFamily: 'Montserrat-Medium',
    fontSize: normalize(18),
    color: '#A3A3A3',
  },
  input: {
    borderBottomWidth: 1,
    borderBottomColor: '#A8A8A8',
    marginBottom: normalize(15),
  },
  picker: {
    color: '#6D6D6D',
  },
  calendar: {
    borderBottomWidth: 1,
    borderBottomColor: '#A8A8A8',
    paddingVertical: normalize(16),
    paddingHorizontal: normalize(16),
    marginBottom: normalize(15),
  },
  photoContainer: {
    marginVertical: normalize(20),
  },
  photo: {
    width: normalize(200),
    height: normalize(200),
    borderRadius: normalize(20),
  },
  date: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(16),
  },
  browseFile: {
    fontFamily: 'Montserrat-Regular',
    fontSize: normalize(15),
    color: '#EBBF1A',
  },
});
