import LottieView from 'lottie-react-native';
import React, {useState} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {FloatingAction} from 'react-native-floating-action';
import {Button, Gap, Header, List} from '../../components';

// const ModalPopUp = ({visible, children}) => {
//   const [showModal, setShowModal] = useState(visible);
//   const scaleValue = useRef(new Animated.Value(0)).current;
//   useEffect(() => {
//     toggleModal();
//   }, [visible]);
//   const toggleModal = () => {
//     if (visible) {
//       setShowModal(true);
//       Animated.spring(scaleValue, {
//         toValue: 1,
//         duration: 300,
//         useNativeDriver: true,
//       }).start();
//     } else {
//       setTimeout(() => setShowModal(false), 200);
//       Animated.timing(scaleValue, {
//         toValue: 0,
//         duration: 300,
//         useNativeDriver: true,
//       }).start();
//     }
//   };

//   return (
//     <Modal transparent visible={showModal}>
//       <View style={styles.modalBackground}>
//         <Animated.View
//           style={[styles.modalContainer, {transform: [{scale: scaleValue}]}]}>
//           {children}
//         </Animated.View>
//       </View>
//     </Modal>
//   );
// };

const RAB = ({navigation}) => {
  // const [visible, setVisible] = useState(false);
  const [data] = useState(true);

  const actions = [
    {
      text: 'Tambah Data',
      icon: require('../../assets/Icons/IcAdd.png'),
      name: 'bt_add',
      position: 1,
      color: '#3D3D3D',
      textStyle: {fontFamily: 'Montserrat-Regular', fontSize: 12},
    },
    {
      text: 'Import Data',
      icon: require('../../assets/Icons/IcImport.png'),
      name: 'bt_import',
      position: 2,
      color: '#3D3D3D',
      textStyle: {fontFamily: 'Montserrat-Regular', fontSize: 12},
    },
  ];
  return (
    <View style={styles.page}>
      {data ? (
        <View style={styles.page}>
          <Header title="Daftar RAB" />
          <View style={styles.content}>
            <ScrollView>
              <List name="3.1.1 Agama Islam" price="Rp. 1.000.000" />
              <List name="3.1.1.2 Bahasa Indonesia" price="Rp. 0" />
              <List name="3.1.1.3 Bahasa Inggris" price="Rp. 100.000" />
            </ScrollView>
            <View style={styles.totalPrice}>
              <Text style={styles.label}>Total Harga</Text>
              <Text style={styles.total}>Rp. 1.100.000</Text>
            </View>
            <Gap height={20} />
            <View style={styles.button}>
              <Button text="Ajukan" color="#181818" />
            </View>
          </View>
        </View>
      ) : (
        <View style={styles.null}>
          <LottieView
            source={require('../../assets/Illustrations/NotFound.json')}
            autoPlay
            loop
            style={styles.illustration}
          />
        </View>
      )}

      {/* Modal Detail */}
      {/* <ModalPopUp visible={visible}>
        <View style={styles.container}>
          <View style={styles.header}>
            <TouchableOpacity onPress={() => setVisible(false)}>
              <Text>A</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ModalPopUp> */}

      {/* Floating Button */}
      <FloatingAction
        actions={actions}
        color="#181818"
        onPressItem={(name) => {
          if (name === 'bt_add') {
            navigation.navigate('AddRAB');
          } else {
            navigation.navigate('ImportRAB');
          }
        }}
      />
    </View>
  );
};

export default RAB;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  content: {
    flex: 1,
    justifyContent: 'space-between',
  },
  button: {
    marginBottom: 100,
    marginHorizontal: 20,
  },
  modalBackground: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContainer: {
    width: '80%',
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 2,
    paddingVertical: 30,
    borderRadius: 20,
    elevation: 20,
  },
  container: {
    alignItems: 'center',
  },
  totalPrice: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 28,
  },
  label: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 18,
    color: '#181818',
  },
  total: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 18,
    color: '#3D3D3D',
  },
  null: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  illustration: {
    width: 350,
  },
});
