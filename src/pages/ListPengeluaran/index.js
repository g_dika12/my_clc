/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useRef, useState} from 'react';
import {
  Animated,
  Modal,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Header, ListRequest} from '../../components';
import {FloatingAction} from 'react-native-floating-action';
import LottieView from 'lottie-react-native';

const ModalPopUp = ({visible, children}) => {
  const [showModal, setShowModal] = useState(visible);
  const scaleValue = useRef(new Animated.Value(0)).current;
  useEffect(() => {
    toggleModal();
  }, [visible]);
  const toggleModal = () => {
    if (visible) {
      setShowModal(true);
      Animated.spring(scaleValue, {
        toValue: 1,
        duration: 300,
        useNativeDriver: true,
      }).start();
    } else {
      setTimeout(() => setShowModal(false), 200);
      Animated.timing(scaleValue, {
        toValue: 0,
        duration: 300,
        useNativeDriver: true,
      }).start();
    }
  };

  return (
    <Modal transparent visible={showModal}>
      <View style={styles.modalBackground}>
        <Animated.View
          style={[styles.modalContainer, {transform: [{scale: scaleValue}]}]}>
          {children}
        </Animated.View>
      </View>
    </Modal>
  );
};

const ListPengeluaran = ({navigation}) => {
  const [visible, setVisible] = useState(false);
  const [data] = useState(true);

  const actions = [
    {
      text: 'Request',
      icon: require('../../assets/Icons/IcAdd.png'),
      name: 'bt_request',
      position: 1,
      color: '#3D3D3D',
      textStyle: {fontFamily: 'Montserrat-Regular', fontSize: 12},
    },
  ];
  return (
    <View style={styles.page}>
      {data ? (
        <View style={styles.page}>
          <Header
            title="List Request"
            onBack
            onPress={() => navigation.goBack()}
          />
          <View style={styles.content}>
            <ScrollView>
              <ListRequest
                name="Beli Buku"
                status="Menunggu Persetujuan"
                onPress={() => setVisible(true)}
              />
              <ListRequest
                name="Beli Kursi"
                status="Disetujui"
                onPress={() => setVisible(true)}
              />
              <ListRequest
                name="Beli Meja"
                status="Ditolak"
                onPress={() => setVisible(true)}
              />
            </ScrollView>
          </View>
        </View>
      ) : (
        <View style={styles.null}>
          <LottieView
            source={require('../../assets/Illustrations/NotFound.json')}
            autoPlay
            loop
            style={styles.illustration}
          />
        </View>
      )}

      {/* Modal Detail */}
      <ModalPopUp visible={visible}>
        <View style={styles.container}>
          <View style={styles.header}>
            <TouchableOpacity onPress={() => setVisible(false)}>
              <Text>A</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ModalPopUp>

      {/* Floating Button */}
      <FloatingAction
        actions={actions}
        color="#181818"
        onPressItem={(name) => {
          navigation.navigate('RequestPengeluaran');
        }}
      />
    </View>
  );
};

export default ListPengeluaran;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  content: {
    flex: 1,
    justifyContent: 'space-between',
  },
  button: {
    marginBottom: 100,
    marginHorizontal: 20,
  },
  modalBackground: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContainer: {
    width: '80%',
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 2,
    paddingVertical: 30,
    borderRadius: 20,
    elevation: 20,
  },
  container: {
    alignItems: 'center',
  },
  null: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  illustration: {
    width: 350,
  },
});
