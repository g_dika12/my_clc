import React, {useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import FilePickerManager from 'react-native-file-picker';
import RNFetchBlob from 'rn-fetch-blob';
import {ILWaiting} from '../../assets';
import {Button, Header} from '../../components';
import {showMessage} from '../../utils';

const ImportRAB = ({navigation}) => {
  const [file, setFile] = useState();

  const BrowseFile = () => {
    FilePickerManager.showFilePicker(null, (response) => {
      if (response.didCancel || response.error) {
        showMessage('Anda tidak memilih File');
      } else {
        // setFile(response);
        console.log(response);
      }
    });
  };
  const donwload = () => {
    RNFetchBlob.config({
      fileCache: true,
      addAndroidDownloads: {
        useDownloadManager: true,
        notification: true,
        title: 'Template RAB',
        description: 'Downloading...',
        path: RNFetchBlob.fs.dirs.DownloadDir + '/RAB.xlsx',
      },
    })
      .fetch(
        'GET',
        'https://docs.google.com/document/d/1CtMnYd0Fbo1sKFcPu_puHVGydPe8oYao/',
      )
      .then((res) => {
        let status = res.info().status;

        if (status === 200) {
          res.path();
        }
      })
      .catch((errorMessage, statusCode) => {
        console.log(errorMessage);
      });
  };
  return (
    <View style={styles.page}>
      <Header title="Import RAB" onBack onPress={() => navigation.goBack()} />
      <View style={styles.container}>
        <View>
          <View style={styles.template}>
            <Text style={styles.label}>Template RAB</Text>
            <TouchableOpacity activeOpacity={0.7} onPress={donwload}>
              <View style={styles.containerTemplate}>
                <Image source={ILWaiting} style={styles.icon} />
                <Text style={styles.text}>Template.xlsx</Text>
              </View>
            </TouchableOpacity>
          </View>
          <TouchableOpacity activeOpacity={0.7} onPress={BrowseFile}>
            <View style={styles.border}>
              <View style={styles.containerFile}>
                {file ? (
                  <Text style={styles.browseFile}>{file}</Text>
                ) : (
                  <Text style={styles.browseFile}>Browse your file</Text>
                )}
              </View>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.button}>
          <Button text="Import" />
        </View>
      </View>
    </View>
  );
};

export default ImportRAB;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  template: {
    paddingHorizontal: 20,
  },
  label: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 16,
    color: '#595959',
  },
  containerTemplate: {
    flexDirection: 'row',
    backgroundColor: '#FEF7DD',
    paddingHorizontal: 18,
    borderRadius: 10,
    marginTop: 11,
    marginBottom: 38,
    alignItems: 'center',
  },
  icon: {
    width: 50,
    height: 50,
    marginRight: 11,
  },
  text: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 14,
  },
  border: {
    marginHorizontal: 20,
    borderWidth: 3,
    borderColor: '#F9D036',
    borderStyle: 'dashed',
    borderRadius: 10,
  },
  containerFile: {
    backgroundColor: '#FEF7DD',
    paddingVertical: 16,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  browseFile: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 15,
    color: '#EBBF1A',
  },
  button: {
    marginHorizontal: 20,
    marginBottom: 67,
  },
});
