import {combineReducers} from 'redux';
import {globalReducer} from './global';
import {saldoReducer} from './saldo';

const reducer = combineReducers({
  globalReducer,
  saldoReducer,
});

export default reducer;
