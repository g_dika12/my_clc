const initState = {
  saldo: [],
};

export const saldoReducer = (state = initState, action) => {
  if (action.type === 'GET_SALDO') {
    return {
      ...state,
      saldo: action.value,
    };
  }
  return state;
};
